package PT2020.demo.Assignment3;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.*;
import com.itextpdf.text.DocumentException;

import connection.ConnectionFactory;
import model.Product;
import presentation.PdfGenerator;
import presentation.ReadFile;
import model.Client;

/**
 * Aceasta este clasa principala care citeste argumentele 
 */
public class App 
{
	/**
	 * Metoda main care creaza un obiect de tip ReadFile avand ca parametru argumentul din linia de comanda
	 * @param args Argument care reprezinta fisierul .txt dat ca date de intrare
	 * @throws SQLException  Exceptii Sql
	 * @throws IOException  Exceptii citire fisier
	 * @throws DocumentException Exceptii scriere in pdf
	 */
    public static void main( String[] args ) throws SQLException, IOException, DocumentException
    {
    ReadFile f=new ReadFile(args[0]);	
    }
}
