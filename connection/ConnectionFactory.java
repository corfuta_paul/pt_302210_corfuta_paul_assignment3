package connection;

import java.sql.*;
/**
 * Clasa care asigura conexiunea la baza de date
 * @author corfu
 *
 */
public class ConnectionFactory {
   
        private static final String DBURL="jdbc:mysql://localhost:3306/assignment3?useSSL=false";
        private static final String USER="root";
        private static final String PASS="root";
		
        private static ConnectionFactory con=new ConnectionFactory(); 

	/**
	 * metoda care creeaza conexiunea
	 * @return returneaza conexiunea
	 * @throws SQLException arunca exceptie in caz ca nu se poate crea conexiunea
	 */
    private Connection createConnection() throws SQLException
    {
    	return DriverManager.getConnection(DBURL,USER,PASS);
    }
    /**
     * apeleaza metoda createConnection()
     * @return returneaza conexiunea
     * @throws SQLException arunca exceptie in caz ca nu se poate crea conexiunea
     */
    public static Connection getConnection() throws SQLException
    {	
         return con.createConnection();
    }
    /**
     * metoda care inchide conexiunea
     * @param conn conexiunea
     * @throws SQLException arunca o exceptie daca nu se poate inchide conexiunea
     */
    public static void close(Connection conn) throws SQLException
    {
    	conn.close();
    }
   /**
    * metoda care inchide un Statement
    * @param st statement
    * @throws SQLException arunca o exceptie daca nu se poate inchide statement-ul
    */
    public static void close(Statement st) throws SQLException
    {
    	st.close();
    }
    /**
     * metoda care inchide ResultSet
     * @param res ResultSet
     * @throws SQLException arunca o exceptie daca nu se poate inchide ResultSet
     */
    public static void close(ResultSet res) throws SQLException
    {
    	res.close();
    }
}
