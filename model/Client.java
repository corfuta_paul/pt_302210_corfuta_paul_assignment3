package model;

import java.sql.*;
import java.util.ArrayList;

import connection.ConnectionFactory;
/**
 * Clasa care se ocupa cu gestionarea informatie despre clienti
 * @author corfu
 *
 */

public class Client {

	private int idClient;
	private String name;
	private String address;
	/**
	 * Constructorul clasei
	 * @param id id-ul clientului
	 * @param name numele clientului
	 * @param address adresa clientului
	 */
	public Client(int id, String name, String address) {
		super();
		this.idClient = id;
		this.name = name;
		this.address = address;
	}
	public int getId() {
		return idClient;
	}
	public void setId(int id) {
		this.idClient = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	private final static String selectAll="Select * From client ";
	private final static String insert="INSERT INTO `assignment3`.`client` ( `Name`, `Address`) VALUES ( ?, ?);";
	private final static String delete="DELETE FROM `assignment3`.`client` WHERE ( `Name` =? and `Address` = ?)" ;
	/**
	 * Metoda care parcurge toata tabela Client 
	 * @return returneaza lista cu toti clientii din tabela
	 * @throws SQLException exceptie
	 */
	public static ArrayList<Client> selectAll() throws SQLException
	{
		Client client=null;
		ArrayList<Client> clients=new ArrayList<Client>();
		Connection conn=ConnectionFactory.getConnection();
		Statement st=conn.createStatement();
		ResultSet res=st.executeQuery(selectAll);
		
		while(res.next())	
	    	 clients.add(new Client(res.getInt("idClient"),res.getString("name"),res.getString("address")));
		ConnectionFactory.close(conn);
		ConnectionFactory.close(st);
		ConnectionFactory.close(res);
		return clients;
	}
	/**
	 * Insereaza un client in baza de date
	 * @param name NumeClient
	 * @param address Adresa
	 * @throws SQLException exceptie
	 */
	public static void insertClient(String name,String address) throws SQLException
	{
		Connection conn=ConnectionFactory.getConnection();
		PreparedStatement st=null;
			
		st=conn.prepareStatement(insert);
		st.setString(1, name);
		st.setString(2, address);
		st.executeUpdate();
		ConnectionFactory.close(conn);
		ConnectionFactory.close(st);
	}
	/**
	 * Sterge un client din baza de date
	 * @param name NumeClient
	 * @param address Adresa
	 * @throws SQLException exceptie
	 */
	public static void deleteClient(String name,String address) throws SQLException
	{
		Connection conn=ConnectionFactory.getConnection();
		PreparedStatement st=null;
			
		st=conn.prepareStatement(delete);
		st.setString(1, name);
		st.setString(2, address);
		st.executeUpdate();
		ConnectionFactory.close(conn);
		ConnectionFactory.close(st);
	}
	public String toString() {
		String s="";
		s+=idClient+","+name+", "+address;
		return s;
	}
}
