package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import connection.ConnectionFactory;
/**
 * Clasa care se ocupa cu gestionarea informatiei despre comenzi
 * @author corfu
 *
 */
public class Order {
     private String clientName;
     private String productName;
     private int cantity;
     private double totalPrice;
     /**
      * Constructorul clasei
      * @param clientName Nume client
      * @param productName Nume produs
      * @param cantity Cantitate
      * @param totalPrice Pret total
      */
	public Order(String clientName, String productName, int cantity, double totalPrice) {
		super();
		this.clientName = clientName;
		this.productName = productName;
		this.cantity = cantity;
		this.totalPrice = totalPrice;
	}
	private final static String selectAll="Select * From `assignment3`.`order`";
	private final static String insert="INSERT INTO `assignment3`.`order` ( `ClientName`, `ProductName`, `Cantity`,`TotalPrice`) VALUES ( ?,?,?,?);";
	/**
	 * Parcurge toate elementele din tabela Order
	 * @return returneaza lista cu toate comenzile
	 * @throws SQLException exceptie
	 */
	public static ArrayList<Order> selectAll() throws SQLException
	{
		ArrayList<Order> orders=new ArrayList<Order>();
		Connection conn=ConnectionFactory.getConnection();
		Statement st=conn.createStatement();
		ResultSet res=st.executeQuery(selectAll);
		
		while(res.next())	
	    	 orders.add(new Order(res.getString("ClientName"),res.getString("ProductName"),res.getInt("Cantity"),res.getDouble("TotalPrice")));
		ConnectionFactory.close(conn);
		ConnectionFactory.close(st);
		ConnectionFactory.close(res);
		return orders;
	}
	/**
	 * Insereaza o comanda noua in tabela
	 * @param clientName Nume client
	 * @param productName Nume produs
	 * @param cantity cantitate
	 * @return returneaza 1 daca comanda are o cantitate mai mica decat cantitatea produsului aflat in stoc, 0 in caz contrar
	 * @throws SQLException -
	 */
	public static int insertOrder(String clientName,String productName,int cantity) throws SQLException
	{
		Connection conn=ConnectionFactory.getConnection();
		PreparedStatement st=null;
		int ok=0;
		double price=0;
		ArrayList<Product> products=new ArrayList<Product>();
		products=Product.selectAll();
		for(int i=0;i<products.size();i++)
			if(products.get(i).getName().equalsIgnoreCase(productName))
			{
				price= products.get(i).getPrice();
				if(cantity<=products.get(i).getCantity())
				{
					ok=1;
					Product.substractProduct(cantity,products.get(i).getName());
				}
				      	
			}
		if(ok==1)
		{
		st=conn.prepareStatement(insert);
		st.setString(1, clientName);
		st.setString(2, productName);
		st.setString(3, String.valueOf(cantity));
		st.setString(4, String.valueOf(cantity*price));
		st.executeUpdate();
		ConnectionFactory.close(conn);
		ConnectionFactory.close(st);
		return 1;
		}
		return 0;	
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getCantity() {
		return cantity;
	}
	public void setCantity(int cantity) {
		this.cantity = cantity;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
}

