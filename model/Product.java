package model;

import java.sql.*;
import java.util.ArrayList;

import connection.ConnectionFactory;
/**
 * Clasa care se ocupa cu prelucrarea informatiei despre produse
 * @author corfu
 *
 */
public class Product {
    private int idProduct;
    private String name;
    private int cantity;
    private double price;
    /**
     * Constructorul clasei 
     * @param idProduct id Produs
     * @param name Nume produs
     * @param cantity Cantitate
     * @param price Pret
     */
	public Product(int idProduct, String name, int cantity, double price) {
		super();
		this.idProduct = idProduct;
		this.name = name;
		this.cantity = cantity;
		this.price = price;
	}
	public int getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCantity() {
		return cantity;
	}
	public void setCantity(int cantity) {
		this.cantity = cantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	private final static String selectAll="Select * From product ";
	private final static String insert="INSERT INTO `assignment3`.`product` ( `NameProduct`, `Cantity`, `Price`) VALUES ( ?,?,?);";
	private final static String delete="DELETE FROM `assignment3`.`product` WHERE ( `NameProduct` =? )" ;
	private final static String update="UPDATE `assignment3`.`product` SET `cantity`=`cantity` + ? Where `nameProduct`=?;";
	private final static String updateCantity="UPDATE `assignment3`.`product` SET `cantity`=`cantity` - ? Where `nameProduct`=?;";
	/**
	 * Parcurge toata tabela Products
	 * @return O lista cu toate produsele
	 * @throws SQLException exceptie
	 */
	public static ArrayList<Product> selectAll() throws SQLException
	{
		
		ArrayList<Product> products=new ArrayList<Product>();
		Connection conn=ConnectionFactory.getConnection();
		Statement st=conn.createStatement();
		ResultSet res=st.executeQuery(selectAll);
		
		while(res.next())	
	    	 products.add(new Product(res.getInt("idProduct"),res.getString("NameProduct"),res.getInt("Cantity"),res.getDouble("Price")));
		ConnectionFactory.close(conn);
		ConnectionFactory.close(st);
		ConnectionFactory.close(res);
		return products;
	}
	/**
	 * verifica daca mai exista produse cu acelasi nume in tabela
	 * @param name Nume produs
	 * @return daca returneaza 1 exista produs cu acelasi nume, 0 nu exista
	 * @throws SQLException -exceptie
	 */
	public static int exist(String name) throws SQLException
	{
		Connection conn=ConnectionFactory.getConnection();
		PreparedStatement st=conn.prepareStatement("select * from product where nameProduct=?");
		ResultSet res=null;
		st.setString(1, name);
		res=st.executeQuery();
		while(res.next())
		{
			
			if(res.getString("NameProduct").equalsIgnoreCase(name))
				return 1;
		}
		
		return 0;
	}
	/**
	 * Insereaza produs in tabela daca nu exista, in caz contrar face update la cantitate
	 * @param name Nume Produs
	 * @param cantity Cantitate
	 * @param price Pret
	 * @throws SQLException exceptie
	 */
	public static void insertProduct(String name,int cantity,Double price) throws SQLException
	{
		Connection conn=ConnectionFactory.getConnection();
		PreparedStatement st=null;
		if (exist(name)==1)
		{
			st=conn.prepareStatement(update);
			st.setInt(1, cantity);
			st.setString(2, name);
			st.executeUpdate();
		}
		else
		{	
		st=conn.prepareStatement(insert);
		st.setString(1, name);
		st.setInt(2, cantity);
		st.setDouble(3, price);
		st.executeUpdate();
		}
		ConnectionFactory.close(conn);
		ConnectionFactory.close(st);
	}
	/**
	 * Sterge produsul din baza de date
	 * @param name Nume
	 * @throws SQLException exceptie
	 */
	public static void deleteProduct(String name) throws SQLException
	{
		Connection conn=ConnectionFactory.getConnection();
		PreparedStatement st=null;
			
		st=conn.prepareStatement(delete);
		st.setString(1, name);
	
		st.executeUpdate();
		ConnectionFactory.close(conn);
		ConnectionFactory.close(st);
	}
	/**
	 * Scade cantitate produselor cu o anume cantitate la produsul a carui nume este name
	 * @param cantity Cantitate
	 * @param name Numele Produsului
	 * @throws SQLException exceptie
	 */
	public static void substractProduct(int cantity,String name) throws SQLException
	{
		Connection conn=ConnectionFactory.getConnection();
		PreparedStatement st=null;
			
		st=conn.prepareStatement(updateCantity);
		st.setInt(1, cantity);
	    st.setString(2, name);
		st.executeUpdate();
		ConnectionFactory.close(conn);
		ConnectionFactory.close(st);
	}
}
