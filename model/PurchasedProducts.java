package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import connection.ConnectionFactory;
/**
 * Clasa care calculeaza cate produse s-au comandat
 * @author corfu
 *
 */
public class PurchasedProducts {

	private String name;
	private int cantity;
	public PurchasedProducts(String name, int cantity) {
		super();
		this.name = name;
		this.cantity = cantity;
	}
	private final static String insert="INSERT INTO `assignment3`.`purchased_products` ( `ProductName`, `Cantity`) VALUES ( ?,?);";
	private final static String update="UPDATE `assignment3`.`purchased_products` SET `cantity`=`cantity` + ? Where `ProductName`=?;";
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCantity() {
		return cantity;
	}
	public void setCantity(int cantity) {
		this.cantity = cantity;
	}
	/**
	 * Daca exista produs adauga la cantitate cantitatea cumparata
	 * @param name Nume produs
	 * @return 1 daca exista, 0 daca nu
	 * @throws SQLException -
	 */
	public static int exist(String name) throws SQLException
	{
		Connection conn=ConnectionFactory.getConnection();
		PreparedStatement st=conn.prepareStatement("select * from purchased_products where ProductName=?");
		ResultSet res=null;
		st.setString(1, name);
		res=st.executeQuery();
		while(res.next())
		{	
			if(res.getString("ProductName").equalsIgnoreCase(name))
				return 1;
		}
		
		return 0;
	}
	/**
	 * Insereaza un produs in tabela Purchased_Products
	 * @param name Nume produs
	 * @param cantity Cantitate
	 * @throws SQLException -
	 */
	public static void insertProduct(String name,int cantity) throws SQLException
	{
		Connection conn=ConnectionFactory.getConnection();
		PreparedStatement st=null;
		if (exist(name)==1)
		{
			st=conn.prepareStatement(update);
			st.setInt(1, cantity);
			st.setString(2, name);
			st.executeUpdate();
		}
		else
		{	
		st=conn.prepareStatement(insert);
		st.setString(1, name);
		st.setInt(2, cantity);
		st.executeUpdate();
		}
		ConnectionFactory.close(conn);
		ConnectionFactory.close(st);
	}
}
