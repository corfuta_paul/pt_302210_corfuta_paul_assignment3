package presentation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Stream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import model.Client;
import model.Order;
import model.Product;
/**
 * Clasa care genereaza documente pdf
 * @author corfu
 *
 */
public class PdfGenerator {

	private static final boolean Client = false;
	public PdfGenerator()  {
		
	}
	/**
	 * Genereaza un document pdf pentru fiecare comanda facuta
	 * @param i numarul comenzii
	 * @param orders comezile
	 * @param name Nume Client
	 * @throws DocumentException -
	 * @throws FileNotFoundException -
	 */
	public static void order(int i,ArrayList<Order> orders,String name) throws DocumentException, FileNotFoundException
	{
    String res="Order"+i+".pdf";
	Document document = new Document();
	PdfWriter.getInstance(document, new FileOutputStream(res));
	document.open();
	for(int count=0;count<orders.size();count++)
		if(name.equalsIgnoreCase(orders.get(count).getClientName()))
			res=orders.get(count).getClientName()+" bought "+orders.get(count).getCantity()+orders.get(count).getProductName()+"s."+"Total price:"+orders.get(count).getTotalPrice() ;
	Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
	Chunk chunk = new Chunk(res, font);
	 
	document.add(chunk);
	document.close();
	}
	/**
	 * Genereaza un document pdf pentru comenzile clientilor care au vrut mai mult decat este in stoc
	 * @param i numarul comenzii
	 * @param orders comenzile
	 * @param name Nume Client
	 * @throws DocumentException -
	 * @throws FileNotFoundException -
	 */
	public static void orderFail(int i,ArrayList<Order> orders,String name,String nameClient) throws DocumentException, FileNotFoundException
	{
    String res="Order"+i+".pdf";
	Document document = new Document();
	PdfWriter.getInstance(document, new FileOutputStream(res));
	document.open();
	for(int count=0;count<orders.size();count++)
		if(name.equalsIgnoreCase(orders.get(count).getProductName()))
			res=nameClient+" tried to buy "+orders.get(count).getProductName()+".Out of stock!" ;
	Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
	Chunk chunk = new Chunk(res, font);
	 
	document.add(chunk);
	document.close();
	}
	/**
	 * Genereaza un document pdf cu situatia tabelei Order
	 * @param i numarul report-ului
	 * @param orders lista de comenzi
	 * @throws FileNotFoundException -
	 * @throws DocumentException -
	 */
	public static void reportOrder(int i,ArrayList<Order> orders) throws FileNotFoundException, DocumentException
	{
		String s="ReportOrder"+i+".pdf";
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(s));
		 
		document.open();
		 
		PdfPTable table = new PdfPTable(4);
		addOrderTableHeader(table);
		addOrderRows(table, orders);
		
		 
		document.add(table);
		document.close();
	}
	/**
	 * genereaza situatia tabelei Client
	 * @param i nr report
	 * @param clients lista clienti
	 * @throws FileNotFoundException -
	 * @throws DocumentException -
	 */
	public static void reportClient(int i,ArrayList<Client> clients) throws FileNotFoundException, DocumentException
	{
		String s="ReportClient"+i+".pdf";
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(s));
		 
		document.open();
		 
		PdfPTable table = new PdfPTable(3);
		addClientTableHeader(table);
		addClientRows(table, clients);
		
		 
		document.add(table);
		document.close();
	}
	/**
	 * Genereaza un document pdf cu situatia tabelei Product
	 * @param i nr report
	 * @param products lista produse
	 * @throws FileNotFoundException -
	 * @throws DocumentException -
	 */
	public static void reportProduct(int i,ArrayList<Product> products) throws FileNotFoundException, DocumentException
	{
		String s="ReportProducts"+i+".pdf";
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(s));
		 
		document.open();
		 
		PdfPTable table = new PdfPTable(4);
		addProductTableHeader(table);
		addProductRows(table, products);
		
		 
		document.add(table);
		document.close();
	}
	/**
	 * Adauga header tabel Client
	 * @param table tabel
	 */
	private static void addClientTableHeader(PdfPTable table) {
	    Stream.of("idClient", "Name", "Address").forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.CYAN);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
	}
	/**
	 * adauga randuri
	 * @param table tabel
	 * @param clients lista clienti
	 */
	private static void addClientRows(PdfPTable table,ArrayList<Client> clients) {
		
		for(int i=0;i<clients.size();i++)
		{
			table.addCell (String.valueOf(clients.get(i).getId()));
			table.addCell (clients.get(i).getName());
			table.addCell (clients.get(i).getAddress());
		}
	}
	/**
	 * adauga header tabel Product
	 * @param table tabel
	 */
	private static void addProductTableHeader(PdfPTable table) {
	    Stream.of("idProduct", "ProductName", "Cantity","Price").forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.PINK);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
	}
	/**
	 * adauga randuri 
	 * @param table tabel
	 * @param products lista produse
	 */
	private static void addProductRows(PdfPTable table,ArrayList<Product> products) {
		
		for(int i=0;i<products.size();i++)
		{
			table.addCell (String.valueOf(products.get(i).getIdProduct()));
			table.addCell (products.get(i).getName());
			table.addCell (String.valueOf(products.get(i).getCantity()));
			table.addCell (Double.toString((products.get(i).getPrice())));
		}
	}
	/**
	 * adauga header tabel Order
	 * @param table tabel
	 */
	private static void addOrderTableHeader(PdfPTable table) {
	    Stream.of("ClientName", "ProductName", "Cantity","TotalPrice").forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.PINK);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
	}
	/**
	 * adauga randuri
	 * @param table tabel
	 * @param orders lista comenzi
	 */
	private static void addOrderRows(PdfPTable table,ArrayList<Order> orders) {
		
		for(int i=0;i<orders.size();i++)
		{
			table.addCell (String.valueOf(orders.get(i).getClientName()));
			table.addCell (orders.get(i).getProductName());
			table.addCell (String.valueOf(orders.get(i).getCantity()));
			table.addCell (Double.toString((orders.get(i).getTotalPrice())));
		}
	}
	
	
}
