package presentation;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

import com.itextpdf.text.DocumentException;

import model.Client;
import model.Order;
import model.Product;
import model.PurchasedProducts;

/**
 * Clasa ReadFile este clasa care prelucreaza datele din fisier
 * @author corfu
 * 
 */


public class ReadFile {
  
	private String commands;
	int indexReportClient=1;
	int indexReportProduct=1;
	int indexReportOrder=1;
	int indexOrder=1;
	/**
	 * Constructorul clase ReadFile care apeleaza functia readAndExecute()
	 * @param commands String-ul care contine informatia din fisier
	 * @throws IOException -
	 * @throws SQLException -
	 * @throws DocumentException -
	 */
	 public ReadFile(String commands) throws IOException, SQLException, DocumentException {
		super();
		this.commands = commands;
		readAndExecute();
		
	}
	 /**
	  * Functia care citeste fisierul si il imparte in mai multe string-uri pentru a putea fi prelucrata informatia
	  * @throws IOException -
	  * @throws SQLException -
	  * @throws DocumentException -
	  */
	public void readAndExecute() throws IOException, SQLException, DocumentException
	{
		
		String data = ""; 
	    data = new String(Files.readAllBytes(Paths.get(commands))); 
	    String[] lines = data.split("\\r?\\n");
        for (String line : lines) {
            String[] s=line.split("[,:]+");
            System.out.println(s[0]);
            if(s[0].equalsIgnoreCase("Insert client"))
            	Client.insertClient(s[1], s[2]);
            else if(s[0].equalsIgnoreCase("Insert product"))
            	Product.insertProduct(s[1],Integer.parseInt(s[2]),Double.parseDouble(s[3]));
            else if(s[0].equalsIgnoreCase("Delete client"))
            	Client.deleteClient(s[1], s[2]);
            else if(s[0].equalsIgnoreCase("Delete product"))
            	Product.deleteProduct(s[1]);
            else if(s[0].equalsIgnoreCase("Report client"))
            	PdfGenerator.reportClient(indexReportClient++,Client.selectAll());
            else if(s[0].equalsIgnoreCase("Report product"))
            	PdfGenerator.reportProduct(indexReportProduct++,Product.selectAll());
            else if(s[0].equalsIgnoreCase("Order"))
            {
            	if(Order.insertOrder(s[1], s[2],Integer.parseInt(s[3]))!=0)
            	{
            	    PdfGenerator.order(indexOrder++, Order.selectAll(),s[1]);
            	    PurchasedProducts.insertProduct(s[2],Integer.parseInt(s[3]) );
            	}
            	else
            		PdfGenerator.orderFail(indexOrder++, Order.selectAll(), s[2],s[1]);
            }
            else if(s[0].equalsIgnoreCase("Report order"))
            {
            	PdfGenerator.reportOrder(indexReportOrder++, Order.selectAll());
            }
        }
	}
}
